ZDOTDIR="${HOME}/.zsh"
PS1="%F{red}%n@%m:%U%~%u%# %f"
RPS1="%F{red}%B%D{%FT%T}%b%f"
HOMEBREW_PREFIX=~/.homebrew

path=()
if [ -x /usr/libexec/path_helper ]; then
	eval `/usr/libexec/path_helper -s`
fi

pre_paths=()
for p in "${(@Qf)$(<~/.paths)}"; do
  pre_paths+="${(e)p}"
  path=("${(@Q)pre_paths}" "${(@Q)path}")
done
path=("${(@uQ)path}")
unset p pre_paths

my_path=(${(@Q)path})

