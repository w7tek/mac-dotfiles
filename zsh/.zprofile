echo 'in .zprofile'

path=(${(@Q)my_path})
unset my_path

fpath+=(~/.zsh ${HOMEBREW_PREFIX}/share/zsh/site-functions)
autoload rm tmux _git _aws _docker

_aws
_docker
_git
