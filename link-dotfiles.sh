#!/bin/sh

ln -sf .dotfiles/gitconfig ~/.gitconfig
ln -sf .dotfiles/gitignore_global ~/.gitignore_global
ln -sf .dotfiles/tmux.conf ~/.tmux.conf
ln -sf .dotfiles/zsh/.zshenv ~/.zshenv
ln -sf .dotfiles/zsh ~/.zsh
ln -sf .dotfiles/paths ~/.paths
[ -d ~/Library/LaunchAgents ] || /bin/mkdir -m 0700 ~/Library/LaunchAgents
ln -sf ~/.dotfiles/launchd/* ~/Library/LaunchAgents/
