# Dotfiles #
____________

### What is this repository for? ###

These are the files that configure my shell and other OS functions the same way I want them, everywhere I work.

### How do I get set up? ###

* Summary of set up
```
git clone https://bitbucket.org/w7tek/mac-dotfiles.git .dotfiles
cd .dotfiles
less ./link-dotfiles.sh  # convince yourself I'm not hacking you.
./link-dotfiles.sh  # when you're convinced, run it.
```

________

Below here, I still am deciding what to do with the Readme.md. Please disregard this stuff.

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact